from datetime import datetime

from sqlmodel import Field, SQLModel


class User(SQLModel, table=True):
    tg_id: int | None = Field(default=None, primary_key=True)
    username: str
    first_login: datetime
