from decouple import config
from fastapi import FastAPI
from sqladmin import Admin, ModelView
from sqlalchemy import create_engine

from .db.models import User

app = FastAPI()

engine = create_engine(config("DB_URL"))
admin = Admin(app, engine)


class UserAdmin(ModelView, model=User):
    column_list = [User.tg_id, User.username]


admin.add_view(UserAdmin)
