from collections.abc import Iterable

from aiogram.types import InlineKeyboardMarkup
from aiogram.utils.keyboard import InlineKeyboardBuilder

from .callbacks import CategoryCallback, SortCallback
from .enums import Category


async def category_menu() -> InlineKeyboardMarkup:
    builder = InlineKeyboardBuilder()
    for category in Category:
        builder.button(text=category, callback_data=CategoryCallback(category=category).pack())
    builder.adjust(3, repeat=True)

    return builder.as_markup()


async def sort_menu(sorts: Iterable[str]) -> InlineKeyboardMarkup:
    builder = InlineKeyboardBuilder()
    for sort in sorts:
        builder.button(text=sort, callback_data=SortCallback(sort=sort).pack())
    builder.adjust(3, repeat=True)

    return builder.as_markup()
