from aiogram.filters.callback_data import CallbackData

from .enums import Category


class CategoryCallback(CallbackData, prefix="category"):
    category: Category


class SortCallback(CallbackData, prefix="sort"):
    sort: str
