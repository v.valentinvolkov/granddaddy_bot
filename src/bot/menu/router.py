from aiogram.enums import ParseMode
from sqlalchemy.ext.asyncio import AsyncEngine
from sqlmodel.ext.asyncio.session import AsyncSession
from sqlmodel import select

from . import keyboards as kb, strings as s

from aiogram import Router
from aiogram.filters import Command
from aiogram.fsm.context import FSMContext
from aiogram.fsm.state import StatesGroup, State
from aiogram.types import Message, CallbackQuery

from .callbacks import CategoryCallback
from db.models import User

router = Router()


class OrderState(StatesGroup):
    category = State()
    sort = State()


@router.message(Command("start"))
async def show_category_menu(message: Message, state: FSMContext):
    """Show sort menu - start of order."""
    await state.set_state(OrderState.category)

    await message.answer(text=s.start_info(), parse_mode=ParseMode.HTML)

    markup = await kb.category_menu()
    text = s.category_menu()
    await message.answer(
        text=text,
        parse_mode=ParseMode.HTML,
        reply_markup=markup
    )


@router.callback_query(CategoryCallback.filter())
async def process_category(query: CallbackQuery, callback_data: CategoryCallback, engine: AsyncEngine):
    """Get chosen "sort" from callback; show next menu."""
    await query.answer()
    category = callback_data.category
    async with AsyncSession(engine) as session:
        statement = select(User.username)
        users = await session.exec(statement)
        print(statement)
    markup = await kb.sort_menu(users)
    text = s.sort_menu()
    await query.message.edit_text(
        text=text,
        parse_mode=ParseMode.HTML,
        reply_markup=markup
    )


@router.callback_query(CategoryCallback.filter())
async def process_sort(query: CallbackQuery, callback_data: CategoryCallback, state: FSMContext, engine: AsyncEngine):
    """Get chosen "sort" from callback; show next menu."""
    await query.answer()
    category = callback_data.category
    with AsyncSession(engine) as session:
        statement = select(User)
        users = session.exec(statement).first()
        print(users)
    markup = await kb.sort_menu(users)
    text = s.sort_menu()
    await query.message.edit_text(
        text=text,
        parse_mode=ParseMode.HTML,
        reply_markup=markup
    )
