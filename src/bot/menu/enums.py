from enum import StrEnum


class Category(StrEnum):
    indica = "Индика"
    sativa = "Сатива"
    hybrit = "Гибрит"
    hybrit_indica = "Гибрит-индика"
    hybrit_sativa = "Гибрит-сатива"
