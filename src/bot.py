import asyncio
from aiogram import Bot, Dispatcher
from decouple import config
from sqlalchemy.ext.asyncio import create_async_engine

from bot.menu import router


async def main():
    bot = Bot(token=config("BOT_TOKEN"))
    dp = Dispatcher(engine=create_async_engine(config("DB_A_URL")))
    dp.include_router(router)

    await dp.start_polling(bot)


if __name__ == '__main__':
    asyncio.run(main())
